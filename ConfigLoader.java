
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;


public class ConfigLoader {

  public static int number_Nodes;//set the basic parameters
  public static  int inter_request_delay;
  public static int  cs_execution_time;
  public static int number_request;
  public static final String COMMENT_SYMBOL = "#"; 
  
  public static String configFile;
 
  public static int  max_Messages;
  private  HashMap<Integer, ProcessAddress> Address;
  private ArrayList<ArrayList<Integer>> neighList;

  public ConfigLoader() {
    Address = new HashMap<>();
    neighList = new ArrayList<>();
  }
 
  // Read configuration file
  // Returned value: neighList
  public  ArrayList<ArrayList<Integer>> readConfigFile(String fileInput,int nodeID) throws IOException {
    int section = 1;
    String line = null;
    int blankCnt=0;
    File iF = new File(fileInput);
    
    if(!iF.exists())
    {
      System.out.println("fileInput does not exist");
      System.exit(0);
    }
    
    FileReader fR = new FileReader(iF);
    BufferedReader bR = new BufferedReader(fR);
  
    //ArrayList<ArrayList<Integer>> neighList = new ArrayList<>();
  
    while ((line = bR.readLine()) != null) {
   
      if (line.isEmpty()) {
        blankCnt++;
        if(blankCnt==1){
          section++;
        }
        continue;
      }
      else{
        blankCnt=0;
      }
      
      if (line.startsWith(COMMENT_SYMBOL)){
        blankCnt=0;
        continue;
      }

      String[] info = line.split("\\s+");

      // Get 4 parameters
      if (section == 1) {
        if(info.length==4){
          number_Nodes = Integer.parseInt(info[0]);
          Message.NODE_SIZE=number_Nodes;
          inter_request_delay= Integer.parseInt(info[1]);
          cs_execution_time=Integer.parseInt(info[2]);
          number_request=Integer.parseInt(info[3]);
        }

        else{
          System.out.println("config file format is wrong_1");
            System.exit(0);
        }
      }

      // All node informaion
      else if(section == 2) {
        if (Address == null){
          Address = new HashMap<>();
        }
        int ID = Integer.parseInt(info[0]);
        ProcessAddress PAddress = new ProcessAddress(ID,  InetAddress.getByName(info[1]), Integer.parseInt(info[2]));
        Address.put(ID, PAddress);
      }

      else{
        System.out.println("config file format is wrong_2");
        System.exit(0);
      }   
       
    }
    // Store neighbors for each node
    //this.number_Nodes = 4;
    //System.out.println("number_Nodes: "+this.number_Nodes);
    for(int j=0 ; j < this.number_Nodes ; j++){
      //nb.clear();
      ArrayList<Integer> nb = new ArrayList<>();
      for(int i=0 ; i < this.number_Nodes ; i++){
        //if(i != j){
          nb.add(i);  
        //}
      }
      //System.out.println(j+":"+nb);
      neighList.add(nb);
      //System.out.println("neighList: "+neighList);
    }
    
    bR.close();
    configFile = fileInput.substring(0, fileInput.lastIndexOf('/'));   ////
    return neighList;
  }

  public  HashMap<Integer, ProcessAddress> getProcessAddress() {
      return Address;
  }
    
  public void printConfig(int nodeID){
      System.out.println(String.format("-------Node %d Configuration-----", nodeID));
      // Print hosts 
      System.out.println("The number of nodes: "+number_Nodes);
      System.out.println("inter_request_delay: "+inter_request_delay);
      System.out.println("cs_execution_time: "+cs_execution_time);
      System.out.println("number_request: "+number_request);
      
      System.out.println("....Node Information....");
      for(int ID=0 ; ID<number_Nodes; ID++){
        System.out.println( Address.get(ID).getProcessID()+ " " + Address.get(ID).getIP()+ " " + Address.get(ID).getPort());
      }
      // System.out.println("Node ID: "+ Address.get(ID).getProcessID());
      // System.out.println("Node Name: "+ Address.get(ID).getIP());
      // System.out.println("Port number: "+ Address.get(ID).getPort());
      System.out.println("Neighbor List: ");
      // for(int ID=0 ; ID<number_Nodes; ID++){
      //   System.out.println(ID+": "+neighList.get(ID));
      // }
      System.out.println(neighList);
      //System.out.println(neighList.get(0));
  }

}


