#!/bin/bash

# Change this to your netid
netid=gxg171430

# Root directory of your project
PROJDIR=/home/010/g/gx/gxg171430/CS6378/AOS3

# Directory where the config file is located on your local system
CONFIGLOCAL=/home/010/g/gx/gxg171430/CS6378/AOS3/config.txt

CONFIGSERVER=/home/010/g/gx/gxg171430/CS6378/AOS3/config.txt
# Directory your java classes are in
BINDIR=$PROJDIR

# Your main project class
PROG=Nain

n=0

cat $CONFIGLOCAL | sed -e "s/#.*//" | sed -e "/^\s*$/d" |
(
    read firstLine
    echo $firstLine

    numberOfServers=$( echo $firstLine | awk '{ print $1}' )

    while [ $n -lt $numberOfServers ]
    do
        read line
        #p=$( echo $line | awk '{ print $1 }' )
        host=$( echo $line | awk '{ print $2 }' )
        #port=$( echo $line | awk '{ print $3 }' ) # port

        xterm -hold -e "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $netid@$host.utdallas.edu java -cp $BINDIR $PROG $n $CONFIGSERVER; exec bash" &

        n=$(( n + 1 ))
    done
   
)
