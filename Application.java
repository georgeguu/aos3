
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;

/**
 *
 */
public class Application implements Runnable {
	private Process proc;
	private String OUTPUT_FILE;

	public Application(Process currentProcess) {
		this.proc = currentProcess;
		this.OUTPUT_FILE = ConfigLoader.configFile + "/" + "output" + currentProcess.getID() + ".txt";
		// this.OUTPUT_FILE = "output" + currentProcess.getID() + ".txt";
		File file = new File(OUTPUT_FILE);
		if (file.exists())
			file.delete();
	}

	@Override
	public void run() {
		try {
			double average_response = 0;

			long startime = System.currentTimeMillis();
			int i = 0;

			/*
			 * Iterator<ProcessAddress> itr = proc.getNeighborAddress().iterator();
			 * 
			 * System.out.print("Neighborlist: "); while(itr.hasNext()) { ProcessAddress
			 * employee = itr.next(); System.out.print("PID:" + employee.getProcessID() +
			 * " "); }
			 */

			while (i < ConfigLoader.number_request) {

				long requst_time = System.currentTimeMillis();
				proc.EnterCS();
				long enter_time = System.currentTimeMillis();

				average_response += enter_time - requst_time;

				System.out.println("\n***********Process " + proc.getID() + " entering CS " + i + " time.**********");
				csExecute();
				System.out.println(
						"\n***********Process " + proc.getID() + " trying to leave CS " + i + " time.**********");
				proc.LeaveCS();
				System.out.println("\n***********Process " + proc.getID() + " leaving CS " + i + " time.**********");

				if (i >= ConfigLoader.number_request - 1)
					;
				else {
					long d = getExp(ConfigLoader.inter_request_delay);
					Thread.sleep(d);
				}

				i++;
			}
			long endtime = System.currentTimeMillis();

			double total = endtime - startime;

			System.out.println("\n\n\nProcess " + proc.getID() + " total " + total + " time.");
			System.out.println("Process " + proc.getID() + " average response time per request "
					+ average_response / ConfigLoader.number_request + " time.");

			// proc.closeChannel();
			// System.exit(0);
			// proc.sendFinishMessage();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private long getExp(int m) {
		return Math.round(-m * Math.log(1 - Math.random()));
	}

	private void csExecute() {

		try {

			printBefore();
			long c = getExp(ConfigLoader.cs_execution_time);
			Thread.sleep(c);

			printAfter();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void printBefore() {
		synchronized (proc.Clock) {
			proc.Clock.tick();

			try {
				PrintWriter pw = new PrintWriter(new FileWriter(OUTPUT_FILE, true));
				pw.println("+ " + proc.getID() + " " + proc.Clock);
				pw.flush();
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void printAfter() {

		try {

			// synchronized (proc.L_clock) {
			// proc.L_clock.tick();
			// }
			// increment mutual exclusion clock

			PrintWriter pw = new PrintWriter(new FileWriter(OUTPUT_FILE, true));
			pw.println("- " + proc.getID() + " " + proc.Clock);
			pw.flush();
			pw.close();

			synchronized (proc.Clock) {
				proc.Clock.tick();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}