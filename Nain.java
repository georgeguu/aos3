
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class Nain {
	public static final int LOAD_DELAY = 30000;
	public static int DEFAULT_Process = 0;
	public static final int INITIAL_DELAY = 20000;

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.print("Invalid Argument ");
			System.exit(0);
		}
		try {
			int Id = Integer.parseInt(args[0]);

			ConfigLoader configLoader = new ConfigLoader();
			ArrayList<ArrayList<Integer>> nList = configLoader.readConfigFile(args[1],Id); // read config file

			ProcessAddress Ad = configLoader.getProcessAddress().get(Id);

			ArrayList<ProcessAddress> nbAddress = new ArrayList<ProcessAddress>();
			ArrayList<ProcessAddress> quoAddress = new ArrayList<ProcessAddress>();
			ArrayList<Integer> neighbors = nList.get(Id);

			int number = neighbors.size(); // store the address of neighbor nodes
			int j = 0;
			while (j < number) {

				int nb = neighbors.get(j);
				ProcessAddress nAdress = configLoader.getProcessAddress().get(nb);

				j++;
				nbAddress.add(nAdress);
				// quoAddress.add(nAdress);
			}

			int n = nList.size(); // store the address of neighbor nodes
			int index = 0;
			while (index < n) {
				if (index != Id) {
					if (nList.get(index).contains(Id) && (!neighbors.contains(index)))

						neighbors.add(index);
				}

				index++;
			}

			j = 0;
			while (j < neighbors.size()) {

				int nb = neighbors.get(j);
				ProcessAddress nAdress = configLoader.getProcessAddress().get(nb);

				j++;
				quoAddress.add(nAdress);
				// quoAddress.add(nAdress);
			}

			Process thisProcess = new Process(Id, Ad);
			thisProcess.setNeighborAddress(nbAddress);

			thisProcess.initializeQuorumFlag();

			new Thread(new ServerThread(thisProcess)).start();// start listening thread
			try {
				// build channel between neighbors
				Thread.sleep(INITIAL_DELAY);

				int size = neighbors.size();

				int i = 0;

				try {
					while (i < size) {
						ProcessAddress nb = nbAddress.get(i);
						// System.out.println(nb.getIP()+" "+nb.getPort());
						Socket cSocket = new Socket(nb.getIP(), nb.getPort());
						thisProcess.addSocketChannel(nb.getProcessID(), cSocket);
						i++;
					}

				} catch (Exception ex) {
					ex.printStackTrace();
				}

				Thread.sleep(LOAD_DELAY);
				new Thread(new Application(thisProcess)).start();

			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
