

import java.net.InetAddress;


public class ProcessAddress {  //store the information of IP address of Process
	private int ProcessID;
    private InetAddress ipAddress;
    private int port;
    
    public ProcessAddress()
    {
    	
    }
    
    public ProcessAddress(int ID, InetAddress IP, int port)
    {
    	this.ProcessID=ID;
    	this.ipAddress=IP;
    	this.port=port;
    }
    
    public int getProcessID()
    {
    	return ProcessID;
    }
    
    public InetAddress getIP()
    {
    	return ipAddress;
    }
    public int getPort()
    {
    	return port;
    }

}
