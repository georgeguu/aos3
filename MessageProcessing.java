
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Iterator;

class MessageProcessing implements Runnable {
	public static final int DEFAULT_PROCESS_ID = 0;
	private ObjectInputStream inputStream;
	private Socket socket;
	private Process proc;

	public MessageProcessing(Socket sk, Process proc) {
		this.socket = sk;
		System.out.println(socket.toString());
		this.proc = proc;

		try {
			inputStream = new ObjectInputStream(socket.getInputStream()); // get input stream from client socket
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void run() {

		try {

			while (true) {
				try {

					boolean flag;

					synchronized (proc.Status) {
						if (proc.Status == CSStatus.CSIN) {
							flag = true;
						} else
							flag = false;
					}

					if (flag == true) {
						synchronized (proc.critical) {
							// System.out.println("wait.....");
							proc.critical.wait();
						}

					}

					// System.out.println("fetche message.....");

					Message inputMs = (Message) inputStream.readObject();

					if (inputMs.getType() == Message.Grant) { // if message is marker message
						// System.out.println("Received Grant message "+inputMs.getSourceID()+"
						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
						handleGRANTMessage(inputMs);
					} else if (inputMs.getType() == Message.Request) { // if message is Application message

						// System.out.println("Received Request message"+inputMs.getSourceID()+"
						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
						handleREQUESTMessage(inputMs);
					}
//					} else if (inputMs.getType() == Message.Fail) { // if message is Application message
//
//						// System.out.println("Received Fail message"+inputMs.getSourceID()+"
//						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
//						handleFAILEDMessage(inputMs);
//					} else if (inputMs.getType() == Message.Inquire) { // if message is Application message
//
//						// System.out.println("Received Inquire message"+inputMs.getSourceID()+"
//						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
//						handleINQUIREMessage(inputMs);
//					}
//
//					else if (inputMs.getType() == Message.Release) { // if message is Application message
//
//						// System.out.println("Received Release message"+inputMs.getSourceID()+"
//						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
//						handleRELEASEMessage(inputMs);
//					}
//
//					else if (inputMs.getType() == Message.Yield) { // if message is Application message
//
//						// System.out.println("Received Yield message"+inputMs.getSourceID()+"
//						// "+inputMs.getDestinationID()+" "+proc.quorumGiven);
//						handleYIELDMessage(inputMs);
//					}
//
//					else if (inputMs.getType() == Message.Finish) { // if message is a Finish message
//						System.out.println("Received Finish message" + inputMs.getSourceID());
//						handleFinishMessage(inputMs);
//
//					}

				}

				catch (EOFException e) {

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		finally { // close the socket finally.
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void handleREQUESTMessage(Message inputMarkMs) {

		System.out.println("Received REQUEST message from " + inputMarkMs.getSourceID() + " with timestamp "
				+ inputMarkMs.getTimestamp());

		synchronized (proc.Clock) {
			proc.Clock.receiveTick(inputMarkMs.getClock());
		}
		synchronized (proc.L_clock) {
			proc.L_clock.receiveTick(inputMarkMs.getTimestamp());
		}

		synchronized (proc.requestPQ) {

			proc.requestPQ.add(inputMarkMs);

			Message request = proc.requestPQ.peek();

			if (request.getSourceID() == proc.getID()) {

				if (proc.receivedAllKeys()) {
					synchronized (proc.grantAll) {
						System.out.println("**************I Got the keys**************");
						proc.requestPQ.poll();
						proc.grantAll.notify();
					}
				}
			} else {

				synchronized (proc.Grant_Hash) {
					proc.Grant_Hash.put(inputMarkMs.getSourceID(), false);
				}

				synchronized (proc.Clock) {
					proc.Clock.tick();
				}
				synchronized (proc.L_clock) {
					proc.L_clock.tick();
				}

				Message a = new Message(proc.getID(), request.getSourceID(), Message.Grant, proc.Clock, proc.L_clock);
				proc.sendMessage(request.getSourceID(), a);
				proc.requestPQ.poll();
				proc.sent_Message_Count++;
				System.out.println("Sent GRANT message to " + request.getSourceID());

				if (proc.haveOwnRequest) {

					synchronized (proc.Clock) {
						proc.Clock.tick();
					}

					synchronized (proc.L_clock) {
						proc.L_clock.tick();
					}

					Message b = new Message(proc.getID(), request.getSourceID(), Message.Request, proc.Clock,
							proc.L_clock);

					proc.sendMessage(request.getSourceID(), b);
					proc.sent_Message_Count++;
					System.out.println("Sent REQUEST message to " + request.getSourceID());
				}

			}

		}

		// }

		// System.out.println("\n--------------Begin printing Request
		// Queue---------------------");
		// synchronized (proc.requestPQ) {

//		Iterator<Message> x = proc.requestPQ.iterator();
//		while (x.hasNext()) {
//			Message e = x.next();
//			System.out.println("Source: " + e.getSourceID() + " Timestamp: " + e.getTimestamp());
//		}
		// }
		// System.out.println("---------------------End print
		// queue----------------------\n");

	}

	public void handleGRANTMessage(Message inputMs) // handle GRANTMessage
	{

		System.out.println(
				"Received GRANT message from " + inputMs.getSourceID() + " with timestamp " + inputMs.getTimestamp());

		// increment clocks
		synchronized (proc.Clock) {
			proc.Clock.receiveTick(inputMs.getClock());
		}

		synchronized (proc.L_clock) {
			proc.L_clock.receiveTick(inputMs.getTimestamp());
		}

		synchronized (proc.Grant_Hash) {
			// true means current process has the shared key
			proc.Grant_Hash.put(inputMs.getSourceID(), true);
		}

		if (proc.receivedAllKeys()) {

			synchronized (proc.requestPQ) {
				proc.requestPQ.poll();
			}

			synchronized (proc.grantAll) {
				System.out.println("**************I Got the keys**************");
				proc.grantAll.notify();

			}
		}
	}

	public void exit() {
		proc.closeChannel();
		System.exit(0);
	}

}