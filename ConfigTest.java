import java.util.ArrayList;

public class ConfigTest {
    
    private static int nodeId;
    private static int portNum;
    
    public static void main(String[] args){
        
        if (args.length != 2) {
            throw new IllegalArgumentException("Please enter port#, node ID and config file path");
        }
        
        nodeId = Integer.parseInt(args[0]);
        System.out.println("nodeID= "+nodeId);

        try{
            ConfigLoader configLoader=new ConfigLoader();
            ArrayList<ArrayList<Integer>> nList = configLoader.readConfigFile(args[1], nodeId);
            configLoader.printConfig(nodeId);
            //System.out.println(nList.get(nodeId));

        }catch (Exception ex) {
                ex.printStackTrace();
            }

        // System.out.println(String.format("-----Node %d Configuration-----", nodeId));

        // // Parse the config.txt
        // Parseconfig config = new Parseconfig(nodeId, args[2]);
        
        // // Print the configuration file
        // //config.loadConfig();
        
        // //System.out.println("Number of nodes: "+ config.getNumOfNode());
   
    }
}
