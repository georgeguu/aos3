import java.io.Serializable;


public class LamportClock implements Comparable<LamportClock>,Serializable {	
    /**
	 * 
	 */
	private static final long serialVersionUID = -6927878679651048041L;
	/**
	 * 
	 */
	
	private int clock;
    private int size;
    private int pid;
    
    
    
    public LamportClock()
    {
    	
    	this.size =ConfigLoader.number_Nodes;
    	//this.clock=new int[size];
    	
    	
    }
    
    
   
    
    public int getTime()
    {
    	return clock;
    }
    
    public LamportClock(int pid, int c)
    {
    	this.size = ConfigLoader.number_Nodes;
    	this.pid = pid;
    	this.clock=c;
    }

    public void tick() { 
    	clock++;
    }
       
    public void receiveTick(int other) {
        //int[] otherVector = other.clock;
        
            this.clock = Math.max(this.clock, other);
        
        this.tick();
    }    
    
    @Override
	
	public int compareTo(LamportClock other) {
		// TODO Auto-generated method stub
    	if (this.clock < other.clock) return -1;
    	else if (other.clock> this.clock) return 1;
    	else {
    		if (this.pid <= other.pid)
    			return -1;
    		else return 1;
    	}
    	
    	
    }
    
}
	
	
