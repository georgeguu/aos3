import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class VectorClockTest {
	
	public static int number_Nodes;
	public static final String LEFT_SYMBOL = "{"; 
	public static final String RIGHT_SYMBOL = "}";
	public static final String COMMENT_SYMBOL = "#";  

	public static void main(String[] args) {
		if (args.length < 1) {
		    System.out.print("Invalid Argument ");
		    System.exit(0);
		}
		int number_Nodes=Integer.parseInt(args[0]);   // nub

		try {
			testClock(number_Nodes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// 
	public static void testClock(int n) throws IOException {
		String output = "TestResult.txt";
		File out = new File(output);
		if(out.exists())
			out.delete();

		PrintWriter pw = new PrintWriter(new FileWriter(out, true));
		Compare(readAlLVectorClk(n), pw);
		pw.flush();
		pw.close();
	}

	// Read All VetorClk.txt files and store all in 'allVetor'
	public static  ArrayList<ArrayList<ArrayList<Integer>>> readAlLVectorClk(int number_Nodes) throws IOException {
		ArrayList<ArrayList<ArrayList<Integer>>> allVector = new ArrayList<ArrayList<ArrayList<Integer>>>();
		int n = number_Nodes;
		for (int i = 0; i<n; i++){
			// System.out.println("ID= "+i);
			// System.out.println(readVectorClkFile(i).size());  // readVectorClkFile(i).size()= (number of entering CS) * 2
			// System.out.println(readVectorClkFile(i));
			allVector.add(readVectorClkFile(i));
		}
		//System.out.println("allVector size: "+allVector.size()); //allVector.size() = number of processes
		return allVector;
	 
	}
	
	// Reading VecotorClk.txt for process Id
	public static ArrayList<ArrayList<Integer>> readVectorClkFile(int Id) throws IOException {
		String input = "output" + Id + ".txt";  //change VC
	  	String eachLine = null; 
	  	
	    File iF = new File(input);
	      
		if(!iF.exists())
		{
		  System.out.println("VectorClk.txt file doesn't exist");
		  System.exit(0);
		}
	      
		FileReader fR = new FileReader(iF);
		BufferedReader bR = new BufferedReader(fR);
	      
	    ArrayList<ArrayList<Integer>> pVectorClock = new ArrayList<>();  // all vetor clock for each process
	    
		while ((eachLine = bR.readLine()) != null) {

		    String[] List=(eachLine.substring(eachLine.indexOf(LEFT_SYMBOL)+1, eachLine.lastIndexOf(RIGHT_SYMBOL)-1)).split(",");
				  
			ArrayList<Integer> vetorClock = new ArrayList<>();  // vetor clcok
		      
			for(int i=0;i<List.length;i++){
			  String nString= List[i]; 
			  if(nString.startsWith(COMMENT_SYMBOL))
			  {
				  break;
			  }
			  else
			  {
				  vetorClock.add(Integer.parseInt(nString));
			  }
				  
			}
			pVectorClock.add(vetorClock);
		}

	      bR.close();

	      //System.out.println(pVectorClock);
	      
	      return pVectorClock;
	  }		 

	// compare all vector clock in specific way and output all they are all concurrently or not
	public static void Compare(ArrayList<ArrayList<ArrayList<Integer>>> allVector, PrintWriter pw) {
		int total=0, right=0, wrong=0;
	
		for (int index1 = 0; index1 < allVector.size(); index1++)
			for (int index2 = index1+1; index2 < allVector.size(); index2++) {
				ArrayList<ArrayList<Integer>> first=	allVector.get(index1);
				ArrayList<ArrayList<Integer>> second = allVector.get(index2);

				//System.out.println("first size: "+first.size()); // first/second size = (number of entering CS) * 2

				boolean noConcurrentClock = true;
				pw.println("-------Compare node " + index1 + " and node " + index2 + "-------");
				total++;

				for (int i = 0; i< first.size(); i++)
				 for (int j = 0; j< second.size(); j++) {
					 if (first.get(i).get(index1) <= second.get(j).get(index1)) 
						 continue;
					 else if (second.get(j).get(index2) <= first.get(i).get(index2)) 
						 continue;
					 else {
						 pw.print("CS Entered concurrently for node " + index1 + ": " + first.get(i).toString());
						 pw.println("and node " + index2 + ": " + second.get(j).toString());
						 noConcurrentClock = false;
						 wrong++; 							 
					 }
					 
				 }				

				if (noConcurrentClock){ 
					pw.println("node " + index1 + " and node " + index2 + " do not enter CS concurrently.");
					right++;
				}

				pw.println();	 
			}

			pw.println("Number of paired comparisons: "+total);    // {[(n-1)+1]*(n-1)}/2
			pw.println("Number of right case: "+right);
			pw.println("Number of cases violating Mutual Exclusion: "+wrong);
	}
	 

}
