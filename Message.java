
import java.io.Serializable;
import java.util.Comparator;

public class Message implements Serializable, Comparable<Message>, Comparator<Message> {

	public static final int Request = 0; // denote type of message
	public static final int Grant = 1;
	public static final int Inquire = 2;
	public static final int Yield = 3;
	public static final int Release = 4;
	public static final int Fail = 5;
	public static final int Finish = 6;

	public static int NODE_SIZE = 5;

	private static final long serialVersionUID = -2723363051271966964L;

	private int srcId;
	private int dstId;
	private String content;

	private int tag;

	private VectorClock ts;

	private int timestamp;

	public Message() {

	}

//	public Message(int tag) {
//		this.tag = tag;
//	}

//	public Message(int desId, int tag) {
//
//		this.dstId = desId;
//		this.tag = tag;
//	}

	
	public Message(String content, int sourceId, int tag) {
		this.content = content;
		this.srcId = sourceId;
		this.tag = tag;
	}

	public Message(String content, int sourceId, int desId, int tag) {
		this.content = content;
		this.srcId = sourceId;
		this.dstId = desId;
		this.tag = tag;
	}
	
	public Message(int sourceId, int desId, int tag, VectorClock ts, LamportClock ls) {

		this.srcId = sourceId;
		this.dstId = desId;
		this.tag = tag;
		this.ts = (VectorClock) ts.clone();
		this.timestamp = ls.getTime();
	}

	public int getTimestamp() {
		return timestamp;
	}

	public VectorClock getClock() {
		return ts;
	}



	public int getType() {
		return tag;

	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public int getSourceID() {
		return srcId;
	}

	public void setSourceID(int sourceId) {
		this.srcId = sourceId;
	}

	public int getDestinationID() {
		return dstId;
	}

	public void setDestinationID(int destId) {
		this.dstId = destId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return content.toString();

	}

	
	
	@Override
    public int compare(Message x, Message y)
    {
        // Assume neither string is null. Real code should
        // probably be more robust
        // You could also just return x.length() - y.length(),
        // which would be more efficient.
		if (x.timestamp < y.timestamp)
			return -1;
		if (x.timestamp > y.timestamp)
			return 1;
		else {
			if (x.srcId <= y.srcId)
				return -1;
			else
				return 1;
		}

    }
	
	@Override
	public int compareTo(Message other) {
		// TODO Auto-generated method stub

		if (this.timestamp < other.timestamp)
			return -1;
		if (this.timestamp > other.timestamp)
			return 1;
		else {
			if (this.srcId <= other.srcId)
				return -1;
			else
				return 1;
		}

		// return this.ts.compareTo(other.ts);
	}

	@Override
	public boolean equals(Object newObj) {

		Message other = (Message) newObj;
		if (this == newObj) {
			return true;
		}

		else {
			if (newObj == null || this.getClass() != newObj.getClass())

			{
				return false;
			}

		}

		if (this.compareTo(other) == 0)
			return true;
		else
			return false;
		// return true;
	}

}
