

import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;




 class ServerThread implements Runnable {
        
        private ServerSocket server;
       
        
        Process proc;

        public ServerThread(Process proc) {
             this.proc=proc;
        }

        @Override
        public void run() {  // start listening 
            try {
               
            	server = new ServerSocket(proc.getLocalPort());
                System.out.print(" Listening from " + proc.getID());
               int index=0;
                while (true) {
                	
                    Socket clientSocket = server.accept();//waiting client socket connect
                    
                   
                    MessageProcessing MP= new MessageProcessing(clientSocket,proc);
                    if(index>proc.getID())
                    {
                    	System.exit(0);
                    }
                    else
                    {
                    	 Thread T=new  Thread(MP);
                    	 
                    	 T.start();  //start message processing thread for the client socket.
                    }
                }
            } catch (Exception ex) {
            	if(ex!=null)
                ex.printStackTrace();
            } 
               
               try {   //close service  socket finally
            	   
            	   if(server!=null)
            	   server.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                
            
        }
    }