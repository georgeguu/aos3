
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;
//import java.util.HashMap;
//import java.util.PriorityQueue;
//import java.util.Random;

public class Process {

	// public static final String OUTPUT_FORMAT = ".out";

	private HashMap<Integer, Socket> hash_Socket;
	private HashMap<Integer, ObjectOutputStream> hash_Output;

	private int ProcessID; // basic variables
	private InetAddress IP;
	private int port;

	public volatile int numberFinished;

	// public volatile boolean haveSendYield;
	public volatile int newGrant;
	public PriorityQueue<Message> requestPQ;

	LinkedList<Message> deferredRequests;
	public volatile boolean haveOwnRequest;

	public volatile CSStatus Status;
	public volatile int sent_Message_Count;
	public VectorClock Clock;
	public LamportClock L_clock;
	public HashMap<Integer, Boolean> Grant_Hash;
	// public volatile int snap_Count = 1;
	public ArrayList<ProcessAddress> neighbor_Address;

	public Object grantAll;
	public Object critical;

	// public volatile boolean haveFailed;

	// public volatile int quorumGiven;
	// public volatile PriorityQueue<Message> InquirePQ;
	// public ConfigLoader configLoader;

	public Process(int ID, ProcessAddress Ad) {
		ProcessID = ID;
		// this.configLoader=conf;

		// IP = Ad.getIP();

		port = Ad.getPort();
		Status = CSStatus.INITIAL;

		// haveFailed = false;
		// haveSendYield = false;
		// quorumGiven = -1;
		// newGrant = 0;
		sent_Message_Count = 0;
		Clock = new VectorClock(ProcessID, ConfigLoader.number_Nodes);
		L_clock = new LamportClock(ProcessID, 0);

		Grant_Hash = new HashMap<>();
		hash_Socket = new HashMap<>();
		hash_Output = new HashMap<>();
		grantAll = new Object();

		critical = new Object();

		// InquirePQ = new PriorityQueue<Message>(ConfigLoader.number_Nodes);
		numberFinished = 0;
		requestPQ = new PriorityQueue<Message>(ConfigLoader.number_Nodes);
		// resetGrantTable();

	}

	/*
	 * public void resetGrantTable() {
	 * 
	 * for (int i = 0; i < ConfigLoader.number_Nodes; i++) {
	 * 
	 * if (i < ProcessID) this.Grant_Hash.put(i, true); else this.Grant_Hash.put(i,
	 * false);
	 * 
	 * }
	 * 
	 * }
	 */

	public int getID() {
		return ProcessID;
	}

	public int getLocalPort() {
		return port;
	}

	public ArrayList<ProcessAddress> getNeighborAddress() {
		return neighbor_Address;
	}

	public int RandomMean(int value) { // produce random number between with mean be value

		Random randomer = new Random();
		int newValue = randomer.nextInt(2 * value) + 1;
		return newValue;
	}

	public void closeChannel() { // close channel
		try {

			for (Integer Id : hash_Output.keySet()) {
				hash_Output.get(Id).close();
				hash_Socket.get(Id).close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Checks if all the keys are received
	public boolean receivedAllKeys() {

		synchronized (this.Grant_Hash) {
			for (boolean b : this.Grant_Hash.values())
				if (!b)
					return false;

			return true;
		}
	}

	public void initializeQuorumFlag() {

		System.out.println("ID: is " + ProcessID);
		Grant_Hash = new HashMap<>();

		int size = neighbor_Address.size();
		int i = 0;

		while (i < size) {
			// if (i != getID()) {
			if (i < this.ProcessID)
				Grant_Hash.put(i, false);
			else
				this.Grant_Hash.put(i, true);
			// }
			System.out.println("Process " + i + " " + Grant_Hash.get(i));
			i++;
		}

	}

	public void sendMessage(int destinationID, Message sendMS) {

		synchronized (hash_Output) {
			try {
				if (hash_Output.size() == 0) {

					System.out.println("without initialization node");
				}
				// System.out.println("destinationNode "+destinationID);

				if (!hash_Socket.get(destinationID).isClosed()) {
					ObjectOutputStream output = hash_Output.get(destinationID);

					if (hash_Output != null) {
						output.writeObject(sendMS);
						output.flush();
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void setNeighborAddress(ArrayList<ProcessAddress> nb) {
		this.neighbor_Address = nb;
	}

	/*
	 * public void sendFinishMessage() {// send finish message to children nodes
	 * 
	 * int size = getNeighborAddress().size(); int index = 0; while (index < size) {
	 * int p_id = getNeighborAddress().get(index).getProcessID(); Message finishMs =
	 * new Message(getID(), p_id, Message.Finish, Clock, L_clock); sendMessage(p_id,
	 * finishMs); index++; }
	 * 
	 * writeResult();
	 * 
	 * }
	 */

	@Override
	public String toString() {// transfer to string format
		StringBuilder sb = new StringBuilder("Process state at  " + getID() + " : {");
		if (Clock != null) {

			sb.append(Clock.toString() + " ");
		}

		sb.append("}");
		return sb.toString();
	}

	public void writeResult() { // output result

	}

	public void addSocketChannel(int ID, Socket clientSocket) {

		hash_Socket.put(ID, clientSocket);

		try {
			hash_Output.put(ID, new ObjectOutputStream(clientSocket.getOutputStream()));
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private void BroadcastRequest() {

		// System.out.println("BroadcastRequest " + getNeighborAddress().size());

		int size = getNeighborAddress().size();
		int index = 0;
		System.out.print("\nBroadcastRequest to: ");

		synchronized (Clock) {
			Clock.tick();
		}
		synchronized (L_clock) {
			L_clock.tick();
		}

		synchronized (Grant_Hash) {
			while (index < size) {

				if (index != this.getID()) {
					if (!Grant_Hash.get(index)) {

						Message requestMs = new Message(this.getID(), index, Message.Request, Clock, L_clock);
						this.sent_Message_Count++;
						sendMessage(index, requestMs);
						System.out.print(index + " ");
					}
				}
				index++;
			}
			System.out.println();
		}

		// send own request to itself
		if (!receivedAllKeys())
			sendMessage(this.getID(), new Message(this.getID(), this.getID(), Message.Request, Clock, L_clock));
	}
	// }
	// }

	public void EnterCS() {

		// deferredRequests = new LinkedList<Message>();

		this.haveOwnRequest = true;
		BroadcastRequest();

		try {
			synchronized (grantAll) {
				// System.out.println("wait for notify");

				if (receivedAllKeys() == false)
					this.grantAll.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Status=CSStatus.CSOUT;
		// System.out.println("Node " + getID() + " entering the CS : " + Clock);

		synchronized (Status) {
			Status = CSStatus.CSIN;
		}

	}

	public void LeaveCS() {

		// reInitialize();
		this.haveOwnRequest = false;
		handleDeferred();
		synchronized (Status) {
			// System.out.println("Process " + getID() + " leaving the CS : " + Clock);
			this.Status = CSStatus.CSOUT;
			// haveFailed = false;
			// haveSendYield = false;
			// newGrant = 0;
		}

		synchronized (critical) {
			this.critical.notifyAll();
		}

		// BroadcastRelease();

		grantAll = new Object();

	}

	public void handleDeferred() {
		synchronized (requestPQ) {

			int i = 0;
			int j = requestPQ.size();

			while (i < j) {

				synchronized (Clock) {
					Clock.tick();
				}
				synchronized (L_clock) {
					L_clock.tick();
				}
				Message pop = requestPQ.poll();
				Message a = new Message(this.getID(), pop.getSourceID(), Message.Grant, Clock, L_clock);
				this.sendMessage(pop.getSourceID(), a);
				this.sent_Message_Count++;
				synchronized (Grant_Hash) {
					Grant_Hash.put(pop.getSourceID(), false);
				}
				i++;
			}

		}
	}
}

enum CSStatus {
	CSIN, CSOUT, CSREQUESTED, INITIAL
}
