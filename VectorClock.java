import java.io.Serializable;

public class VectorClock implements Comparable<VectorClock>,Serializable {	
    /**
	 * 
	 */
	private static final long serialVersionUID = -6927878679651048041L;
	/**
	 * 
	 */
	
	private int[] clock;
    private int size;
    private int pid;
    
    
    public VectorClock clone()
    {
    	return new VectorClock(pid,clock.clone());
    }
    public VectorClock()
    {
    	
    	this.size =ConfigLoader.number_Nodes;
    	this.clock=new int[size];
    	
    	
    }
    
    
    public VectorClock(int pid, int size)
    {
    	this.size = size;
    	this.pid = pid;
    	this.clock=new int[size];
    }
    
    public int getTime()
    {
    	return clock[pid];
    }
    
    public VectorClock(int pid, int[] c)
    {
    	this.size = ConfigLoader.number_Nodes;
    	this.pid = pid;
    	this.clock=c;
    }

    public void tick() { 
    	clock[pid]++;
    }
       
    public void receiveTick(VectorClock other) {
        //int[] otherVector = other.clock;
        for(int i = 0; i < this.size; i++){
            this.clock[i] = Math.max(this.clock[i], other.clock[i]);
        }
        this.tick();
    }    
    
    @Override
	/* 
	 * return -1 if this < other;
	 	return 1 if other < this;
	 	return 0 if concurrent.
	 	assume at any given time, will never compare the same vector clock.
	 */
	public int compareTo(VectorClock other) {
		// TODO Auto-generated method stub
    	if (this.clock[pid] <= other.clock[pid]) return -1;
    	else if (other.clock[other.pid] <= this.clock[other.pid]) return 1;
    	else {
    		if (this.pid < other.pid)
    			return -1;
    		else return 1;
    	}
    	
    	
    }
    
    @Override
    public String toString()
    {
    	  StringBuilder sb = new StringBuilder("Timestamp at  " + " : {");
          for (int t: this.clock) {
        	  sb.append( Integer.toString(t) + ",");
          }
          
          sb.append("}");
          return sb.toString();
      }
    }
    
    
    
    

//    	if (this.pid != other.pid) {
//    		if (this.clock[pid] <= other.clock[pid]) return -1;
//    		else if (other.clock[other.pid] <= this.clock[other.pid]) return 1;
//    		else 
//                {
//    			if (this.clock[pid] < other.clock[pid]) return -1;
//        		else return 1; 			
//                        }
//    	}
//    	else {
//    		if (this.clock[pid] < other.clock[pid]) return -1;
//    		else return 1;
//    	}
//	}

	
	

	

